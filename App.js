import React from 'react';
import { SwitchNavigator, StackNavigator, createStackNavigator } from 'react-navigation';

import Camera from './screens/Camera';
import Preview from './screens/Preview';

export default createStackNavigator(
  {
    Camera: Camera,
    Preview: Preview,
  },
  {
    initialRouteName: 'Camera',
  }

);