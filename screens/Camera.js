import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  Modal,
  ActivityIndicator
} from 'react-native';

import RNFetchBlob from 'rn-fetch-blob';

import { withNavigation } from 'react-navigation';

import Camera from 'react-native-camera-android-simple';

import { MaterialDialog } from 'react-native-material-dialog';

import axios, { post } from 'axios';

const Type = {
  FRONT: 'front',
  BACK: 'back'
};

const url = 'http://192.168.1.101/cashingAppWebApi/public/api/v1/products';

class CameraComponent extends Component {
  constructor(props) {
    super(props);
    this._onCapturePress = this._onCapturePress.bind(this);
    this._onReversePress = this._onReversePress.bind(this);

    this.state = {
      type: Type.BACK,
      direccion: 'direccion',
      visibleBefore: true,
      visibleAfter: false,
      loading: false,
    };
    
  }

  

  _onReversePress() {
    const { type } = this.state;
    this.setState({
      type: type === Type.BACK ? Type.FRONT : Type.BACK
    });
  }



  _onCapturePress() {
    const options = {};
    this.setState({ loading: true });
    this.camera.capture({ metadata: options })
      .then((data) => {
        this.setState({direccion: data.path})
        console.log(data);
        this.setState({ loading: false });
        this.setState({ visibleAfter: true });

        let imageUpload = {
          uri: this.state.direccion,
          type: 'image/jpeg',
          name: 'prueba.jpg'
        }

        console.log(imageUpload);
        var data2 = new FormData();
        data2.append('imagen', imageUpload);
        axios.post(url,
          data2,
          {
            headers: {
              'accept': 'application/json',
              'content-type': 'multipart/form-data'
            }
          }
        ).then(resp => console.log(resp))
        .catch(e => console.log(e));
        
      })
      .catch((err) => {
        console.warn('error al guardar')
      });
  }

  _renderCameraButtons() {
    return (
      <View
        style={styles.captureContainer}
        accessibilityLabel="Camera_Container"
      >
        <View style={styles.captureChildContainer} />
        <View style={styles.circleBorder}>
          <Button
            onPress={()=>this._onCapturePress()}
            title="Tomar imagen"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
      </View>
    );
  }

  render() {
    const { type } = this.state;
    return (
      <View style={styles.container}>
        <MaterialDialog
          title="Toma tu imagen de pertada"
          visible={this.state.visibleBefore}
          onOk={() => this.setState({ visibleBefore: false })}
          onCancel={() => this.setState({ visibleBefore: false })}
          cancelLabel= 'CANCELAR'
          okLabel='OK'>
          <Text style={styles.dialogText}>Por favor toma una foografia para la aportada de tu producto</Text>
        </MaterialDialog>
        <MaterialDialog
          title="¿Te gusta esta foto para tu portada?"
          visible={this.state.visibleAfter}
          onOk={() => {this.setState({ visibleAfter: false });this.props.navigation.navigate('Preview');}}
          onCancel={() => this.setState({ visibleAfter: false })}
          cancelLabel= 'TOMAR OTRA FOTO'
          okLabel='OK'>
          <View style={{alignContent:'center',alignItems:'center'}}>
            <Image style={styles.image} source={{uri: this.state.direccion}} />
          </View>
        </MaterialDialog>
        <View style={styles.preview}>
          <Camera
            ref={(instance) => {this.camera = instance}}
            style={styles.preview}
            type={type}/>
            {this._renderCameraButtons()}
        </View>
        {/*Modal de loading*/}
			  {this.state.loading &&
			    <Modal
			      animationType="fade"
			      transparent={true}
			      visible={this.state.loading}
			      onRequestClose={ () => console.log('on request') }>
			      <View style={styles.modalDialog}>
				      <ActivityIndicator 
				        size="large" 
				        color="#F44336" 
				        animating={true}
				        style={{alignSelf: 'center'}} />
				    </View>
			    </Modal>
			    }
      </View>
    );
  }
}

export default withNavigation(CameraComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  preview: {
    flex: 1 
  },
  captureContainer: {
    position: 'absolute',
    flexDirection: 'row',
    right: 10,
    bottom: 10,
    alignItems: 'center',
    zIndex: 1
  },
  captureChildContainer: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width:  150,
    height: 150
  },
  modalDialog: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)'
}
});